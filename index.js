console.log("Hello World")

// [SECTION] Condition Statement
// If, eslse if, else clause

let numG = -1;

// If statement 
// Execures a statement if a specific condition is true

if(numG < 0){
	console.log("Hello");
}

if(numG < 0){
	console.log("Hello");
}

let numH = 1;

// else if clause


// 
// 

if(numG > 0){               //false
	console.log("Hello");
}else if(numH > 0){			//true
	console.log("World");
}

// else statement

// If all other conditions are false
// 
// 

if (numG > 0) {				//false
	console.log("Hello");
}else if (nmuH = 0) {		//false
	console.log("World")
}else{
	console.log("Again")
}

// If, else if, else statemnet with a function

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed){

	if (windSpeed < 30){
		return"Not a typhoon yet.";
	}else if (windSpeed <= 61){
		return"Tropical depression detected.";
	}else if (windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected.";
	}else if (windSpeed >= 89 && windSpeed <= 177){
		return "Severe tropical storm detected.";
	}else{
		return "Typhoon detected.";
	}

}

//Returns the string to the variable "message" that invoked it
message = determineTyphoonIntensity(69);
console.log(message);

message = determineTyphoonIntensity(27);
console.log(message);

message = determineTyphoonIntensity(200);
console.log(message);

message = determineTyphoonIntensity(100);
console.log(message);

message = determineTyphoonIntensity(178);
console.log(message);

if(message == "Tropical storm detected.") {
	console.warn(message);
}

message = determineTyphoonIntensity(75);
console.log(message);

if(message == "Tropical storm detected.") {
	console.warn(message);
}


//[SECTION] Truthy and Falsy



// truthy examples
if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}

if([]){
	console.log("Truthy");
}


// falsy examples
if(false){
	console.log("Falsy");
}

if(0){                         //0==false, that's why walang lalabas na output
	console.log("Falsy");
}

if(undefined){
	console.log("Falsy");
}

// [SECTION] Conditional (Ternary) Operator

// 
// 

// Singlr Statement execution
let ternaryResult =  (1 < -1) ? true : false;   
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple Statement
let name;

function isOfLegalAge(){
	name = "John";
	return "You are of legal age limit";
}

function isUnderAge(){
	name = "Jane";
	return "You are under age limit";
}

let age = parseInt(prompt("What is your age?"));
// parseInt  ---> converts a string number into an Integer
console.log(age)

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge()
console.log("Result of Ternary Operator in function: " + legalAge + ", " + name);

// [SECTION] Switch Statement

// Switch (expression) {
// 		case value;
	// 		statement;
	// 		break;
// 		default;
	// 		statement;
	// 		break;
// }

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case "monday":
		console.log("The color of the day is red");
		break;
	case "tuesday":
		console.log("The color of the day is orange");
		break;
	case "wednesday":
		console.log("The color of the day is yellow");
		break;
	case "thursday":
		console.log("The color of the day is green");
		break;
	case "friday":
		console.log("The color of the day is blue");
		break;
	case "saturday":
		console.log("The color of the day is indigo");
		break;
	case "sunday":
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day.")
		break;
}

// [SECTION] Try-Catch-Finally Statement

function showIntensityAlert(windSpeed){
	try{
		// attempt to execute the code
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert.");
	}
}

showIntensityAlert(56);